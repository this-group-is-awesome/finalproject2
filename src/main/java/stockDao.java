/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.krorawitt.finalproject.Testselection;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;
import dao.DAO_Interface;

public class StockDAO implements DAO_Interface<Stock> {

    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Stock (StockName,StockAmount,StockPrice)VALUES (?,?,? );";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getStockName());
            stmt.setInt(2, object.getStockAmount());
            stmt.setDouble(3, object.getStockPrice());

            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT StockID,StockName,StockAmount,StockPrice FROM Stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int StockID = result.getInt("StockID");
                String StockName = result.getString("StockName");
                int StockAmount = result.getInt("StockAmount");
                double StockPrice = result.getDouble("StockPrice");
                Stock Stock = new Stock(StockID, StockName, StockAmount, StockPrice);
                list.add(Stock);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT StockID,StockName,StockAmount,StockPrice FROM Stock WHERE id = ?" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int StockID = result.getInt("StockID");
                String StockName = result.getString("StockName");
                int StockAmount = result.getInt("StockAmount");
                double StockPrice = result.getDouble("StockPrice");
                Stock Stock = new Stock(StockID, StockName, StockAmount, StockPrice);

                return Stock;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Stock SET StockName = ?,StockAmount = ?,StockPrice = ? WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getStockName());
            stmt.setInt(2, object.getStockAmount());
            stmt.setDouble(3, object.getStockPrice());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Stock WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

}
