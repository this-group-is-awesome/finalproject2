/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.krorawitt.finalproject.Testselection;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author OS
 */
public class ProductDAO implements DAO_Interface<Product> {

    private static Product totalProduct;

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Product (Name, Price) VALUES (?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ID, Name, Price FROM Product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("ID");
                String name = result.getString("Name");
                double price = result.getDouble("Price");
                Product product = new Product(id, name, price);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ID, Name, Price FROM Product WHERE ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("ID");
                String name = result.getString("Name");
                double price = result.getDouble("Price");
                Product product = new Product(pid, name, price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Product checkProduct(ArrayList<Product> list, String name, double price) {

        for (Product product : list) {
            if (product.getName().equals(name)) {
                totalProduct = product;
                return product;
            }

        }

        return null;
    }

    public static Product getTotalProduct() {
        return totalProduct;
    }

    public Product getName(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ID, Name, Price FROM Product WHERE ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("ID");
                String name = result.getString("Name");
                double price = result.getDouble("Price");
                Product product = new Product(pid, name, price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Product WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Product SET Name = ?, Price = ? WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        ProductDAO dao = new ProductDAO();
        System.out.println("Product getAll: " + dao.getAll());
        System.out.println("Product get id: " + dao.get(1));
        int id = dao.add(new Product(-1, "Ice Chocolate", 60.0));
        System.out.println("Product get new id: " + dao.get(id));

//        Product lastProduct = dao.get(4);
//        System.out.println("last product: " + lastProduct);
//        lastProduct.setPrice(50);
//        int row = dao.update(lastProduct);
//        Product updateProduct = dao.get(4);
//        System.out.println("update product: " + updateProduct);
//        dao.delete(8);
    }

}

