/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.krorawitt.finalproject.Testselection;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author cherz__n
 */
public class CustomerDAO implements DAO_Interface<Customer> {
    private static Customer currentCustomer;

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Custommer (Name,Phonenumber,Cusadd) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhonnum());
            stmt.setString(3, object.getCusadd());

            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT ID,Name,Phonenumber,Cusadd FROM Custommer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String phonenumber = result.getString("phonenumber");
                String cusadd = result.getString("cusadd");
                Customer Custommer = new Customer(id, name, phonenumber, cusadd);
                list.add(Custommer);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT ID,Name,Phonenumber,Cusadd FROM Custommer WHERE ID = ?" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String phonenumber = result.getString("phonenumber");
                String cusadd = result.getString("cusadd");
                Customer Custommer = new Customer(pid, name, phonenumber, cusadd);
                return Custommer;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Custommer WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Custommer SET Name = ?,Phonenumber = ?,Cusadd = ? WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhonnum());
            stmt.setString(3, object.getCusadd());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    /////////////////////////////
    public static Customer checkMember(ArrayList<Customer> list,String name, String tel) {
        
        for (Customer customer : list) {
            if (customer.getPhonnum().equals(tel)) {
                currentCustomer = customer; 
                return customer;
            }
           
        }

        return null;
    }
    
    public static Customer getCurrentCustomer() {
        return currentCustomer ;
    }
    ////////////////////////////////
    public static void main(String[] args) {
        CustomerDAO dao = new CustomerDAO();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "KafaeYen", "gg", "444"));
        System.out.println("id: " + id);
        Customer updateProduct = dao.get(id);
        System.out.println("updateproduct:" + updateProduct);

    }
}
