/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author OS
 */
public class User {
    
    private int userId;
    private String userName;
    private String password;

    public User(int UserId, String Name, String Password) {
        this.userId = UserId;
        this.userName = Name;
        this.password = Password;
    }
    
    public String getUsername() {
        return userName;
    }
    
    public String getPassword() {
        return password;
    }
}
